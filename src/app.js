import router from './router/index.js';
import Route from './router/Route.js';

import usersView from './views/users.js';
import userView from './views/user.js';
import reposView from './views/repos.js';

const routes = [
    new Route({
        meta: { title: { text: 'Users' }, navNode: '/users' },
        name: 'users',
        path: '/',
        component: usersView
    }),
    new Route({
        meta: {
            title: {
                text: 'Repos',
                dynamicKey: 'username'
            },
            navNode: '/repos'
        },
        name: 'repos',
        path: '/users/:username/repos',
        component: reposView
    }),
    new Route({
        meta: {
            title: {
                text: '',
                dynamicKey: 'username'
            },
            navNode: '/users'
        },
        name: 'user',
        path: '/users/:username',
        component: userView
    }),

    new Route({
        meta: { title: { text: 'Users' }, navNode: '/users' },
        name: 'users',
        path: '/users',
        component: usersView
    })
];
router(routes);