import { gitFetchRequest } from '../http.js';
import userComponent from '../components/user.js';
import repos from '../components/repos/repos.js';

export default async ({ username, page }) => {
    const getUser = async () => {
        let user = await gitFetchRequest(`users/${username}`);

        window.userName = user.login;

        return userComponent({ user, showDescription: true });
    };

    const user = await getUser();

    return (`
        <div class="single-user-wrapper">
            <div class="user-header">
                <div class="back">
                    <a href="#/users">
                        <span class="arrow prev-arrow">&#10229;</span>
                        <span class="text prev-text">back to users</span>
                    </a>
                </div>
            </div>
            <div class="user-info">
                ${user}
            </div>
            <div class="user-repos">
                ${await repos({ username: username, showSearch: false, page: page })}
            </div>
        </div>
    `);
}