import repos from '../components/repos/repos.js';

export default ({ username, page, searchText }) => repos({
    username,
    showSearch: true,
    page,
    searchText
})