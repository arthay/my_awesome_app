import { gitFetchRequest } from '../http.js';
import pagination from '../components/pagination.js';
import search from '../components/search.js';
import userComponent from '../components/user.js';

export default async ({ searchText = '', page = 1 }) => {
    window.userName = null;

    let totalCount = 0;
    const getUsers = async () => {
        const params = {
            q: 'type:user',
            page,
            per_page: 20
        };

        if(searchText) {
            params.q = `${searchText}`;
        }

        let data = await gitFetchRequest('search/users', params);
        totalCount = data.total_count;

        return data.items.reduce((acc, user) => acc += userComponent({ user }), '');
    };

    const users = await getUsers();
    return (`
        <div class="users-wrapper">
            <div class="users-header">
                ${search({ searchText, className: 'search-users', placeholder: 'Search for users' })}
                ${pagination({ page, totalCount })}
            </div>
            <div class="users-list">
                ${users}
            </div>
            <div class="user-footer">
                ${pagination({ page, totalCount })}
            </div>
        </div>
    `);
}