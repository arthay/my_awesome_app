const RESPONSE_STATUS_TYPES = {
    success: 200,
    created: 201,
    accepted: 202,
    noContent: 204,
    resetContent: 205,
    multipleChoices: 300,
    movedPermanently: 301,
    badRequest: 400,
    unauthorized: 401,
    forbidden: 403,
    notFound: 404,
    internalServerError: 500
};

const gIT_API_URL = 'https://api.github.com/';

export {
    RESPONSE_STATUS_TYPES,
    gIT_API_URL
};