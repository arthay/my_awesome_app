import events from '../event-listeners/index.js';

export default class Router {
    constructor(routes, nodeElement) {
        this.routes = routes;
        this.nodeElement = nodeElement;
        this.navigate(`${location.pathname}${location.hash}`);
    }

    match(route, requestPath) {
        let paramNames = [];
        let regexPath = route.path.replace(/([:*])(\w+)/g, (full, colon, name) => {
            paramNames.push(name);
            return '([^\/]+)';
        }) + '(?:\/|$)';

        let params = {};
        let routeMatch = requestPath.match(new RegExp(regexPath));
        if(routeMatch !== null) {
            params = routeMatch
                .slice(1)
                .reduce((params, value, index) => {
                    if(params === null) params = {};
                    params[paramNames[index]] = value;
                    return params;
                }, null);
        }

        route.setProps(params);

        return routeMatch;
    }

    async navigate(path) {
        const route = this.routes.filter(route => this.match(route, path))[0];
        if(!route) this.nodeElement.innerHTML = "404! Page not found";
        else {
            await this.render(path, route);
            const navElements = document.querySelectorAll('[route]');

            navElements.forEach(navEl => {
                navEl.classList.remove('active');
            });

            const navEl = document.querySelector(`[route='${route.meta.navNode}']`);
            navEl.classList.add('active');
        }
    }

    async render(path, route) {
        window.location.href = path.search('/#') === -1 ? '#' + path : path;
        this.nodeElement.innerHTML = await route.renderView();

        if(events[route.name]) {
            events[route.name](this, path, route);
        }

    }
}