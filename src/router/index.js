import Router from './Router.js';

export default routes => {

    const router = new Router(routes, document.getElementById('app'));

    document.addEventListener('DOMContentLoaded', () => {

        document.querySelectorAll('[route]')
            .forEach(route => route.addEventListener('click', ev => {
                ev.preventDefault();
                let route = ev.target.getAttribute('route');

                if(route === '/repos') {
                    if(!window.userName) {
                        return;
                    } else {
                        route = `/users/${window.userName}/repos`;
                    }
                }
                router.navigate(route);
            }, false));
    });

    window.addEventListener('hashchange', ev => router.navigate(ev.target.location.hash.substr(1)));
}