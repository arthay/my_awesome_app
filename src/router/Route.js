export default class Route {
    constructor({meta, name, path, component}) {
        this.meta = meta;
        this.name = name;
        this.path = path;
        this.component = component;
        this.props = {}
    }

    setProps(newProps) {
        if(newProps) {
            this.props = newProps;
        }
    }

    renderView() {
        let title = this.meta.title.text;

        if(this.meta.title.dynamicKey) {
            title = `${this.props[this.meta.title.dynamicKey]} ${title}`
        }
        document.title = `${title} | My awesome application`;
        return this.component(this.props);
    }
}