export function paginateHandler(routerContext, path, route) {
    let arrowElements = document.querySelectorAll('.pagination-item');
    arrowElements.forEach(arrowEl => arrowEl.addEventListener('click', async ev => {
        let { target } = ev;

        if(!target.classList.contains('pagination-item')) {
            target = target.closest('.pagination-item');
        }

        const page = +target.getAttribute('page');
        const totalCount = +target.getAttribute('totalCount');
        const perPage = +target.getAttribute('perPage');

        if(page < 1 || (totalCount && page > Math.ceil(totalCount / perPage))) {
            return false;
        }

        const props = route.props;
        route.setProps({
            ...props,
            page
        });

        await routerContext.render.call(routerContext, path, route);
    }, false));
}

export function searchInputHandler(routerContext, path, route) {
    let searchElement = document.querySelector('.search-text .search');

    searchElement.addEventListener('input', (ev) => {
        if(window.timer) {
            clearTimeout(window.timer);
        }

        window.timer = setTimeout(async () => {
            const props = route.props;
            route.setProps({
                ...props,
                searchText: ev.target.value
            });

            await routerContext.render.call(routerContext, path, route);

            searchElement = document.querySelector('.search-text .search');

            searchElement.focus();
            searchElement.setSelectionRange(ev.target.value.length, ev.target.value.length);
        }, 500);
    });
}