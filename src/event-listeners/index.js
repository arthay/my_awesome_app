import usersEvents from './users.js';
import userEvents from './user.js';
import reposEvents from './repos.js';

export default {
    users: usersEvents,
    user: userEvents,
    repos: reposEvents
}