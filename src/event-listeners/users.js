import { paginateHandler, searchInputHandler } from './events.js';

export default (routerContext, path, route) => {
    searchInputHandler(routerContext, path, route);
    paginateHandler(routerContext, path, route);
}