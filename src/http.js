import {
    RESPONSE_STATUS_TYPES as responseStatusTypes,
    gIT_API_URL as gitApiURL
} from './utils/costants/http.js';

export function gitFetchRequest(url, data, method = 'GET') {
    return new Promise(async (resolve, reject) => {
        url = `${gitApiURL}${url}`;
        let body = null;
        let responseData;

        if(method === 'GET' && data) {
            url = serializeQueryParams(url, data);
        } else if(method !== 'HEAD') {
            body = JSON.stringify(data);
        }

        try {
            let response = await fetch(url, {
                method: method,
                body,
                headers: {
                    Authorization: 'token 520bdaaaf32d7955002e5d6775c8479fa6b9a8e9'
                }
            });

            if(method !== 'HEAD') {
                responseData = await response.json();
            }

            if(!(response.status >= responseStatusTypes.success && response.status < responseStatusTypes.multipleChoices)) {
                let error = new Error(response.statusText);
                error.response = responseData;
                error.status = response.status;
                reject(error);
            } else {
                if(response.status === responseStatusTypes.noContent || response.status === responseStatusTypes.resetContent) {
                    responseData = null;
                }
            }

            resolve(responseData);
        } catch(error) {
            reject(error);
        }
    });

    function serializeQueryParams(url, paramsObj) {
        url = new URL(url);

        for(const key in paramsObj) {
            if(paramsObj.hasOwnProperty(key)) {
                url.searchParams.append(key, paramsObj[key]);
            }
        }

        return url;
    }
}