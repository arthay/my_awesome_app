export default ({ searchText, className, placeholder }) => (`
    <div class="search-text ${className ? className : ''}">
        <input 
            placeholder="${placeholder}" 
            type="text" 
            value="${searchText}" 
            class="search"
        >
    </div>
`)