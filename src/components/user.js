export default ({ user, showDescription }) => {
    const userDescription = showDescription
        ? (`
            <div class="user-description">
                ${user.bio ? user.bio : 'Info dont exist'}
            </div>
        `)
        : '';

    return (`
        <div class="user">
            <a href="#/users/${user.login}">
                 <div class="user-avatar">
                    <img src="${user.avatar_url}" alt="${user.login}">
                </div>
                <div class="username">
                    <h2>${user.login}</h2>
                </div>
            </a>
        </div>
        ${userDescription}
    `);
}