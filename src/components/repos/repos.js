import { gitFetchRequest } from '../../http.js';
import pagination from '../pagination.js';
import search from '../search.js';
import repoComponent from './repo.js';

export default async ({ username, showSearch, page = 1, searchText = '', per_page = 20 }) => {
    let totalCount = 0;
    const headerFirstEl = showSearch
        ? search({ searchText, className: 'search-repos', placeholder: 'Search for repos' })
        : (`
            <p>Users repositories</p>
        `);

    const getRepos = async () => {
        const params = {
            q: `user:${username}`,
            page,
            per_page: per_page
        };

        if(searchText) {
            params.q = `${searchText}+user:${username}`;
        }

        let data = await gitFetchRequest('search/repositories', params);
        totalCount = data.total_count;

        return data.items.reduce((acc, repo) => acc += repoComponent({ repo }), '');

    };
    const repos = await getRepos();
    return (`
        <div class="repos-wrapper">
            <div class="repos-header">
                ${headerFirstEl}
                ${pagination({ page, totalCount })}
            </div>
            
            <div class="repos-main">
                ${repos}
            </div>
            <div class="repos-footer">
                ${pagination({ page, totalCount })}
            </div>
        </div>
    `);
}