export default ({ repo }) => (`
    <div class="repo-wrapper">
        <a href="${repo.html_url}" target="_blank">
            ${repo.name}
        </a>
    </div>    
`)