export default ({ page, totalCount, perPage = 20 }) => (`
    <div class="pagination">
        <div 
            class="prev pagination-item ${page === 1 ? 'disabled' : ''}" 
            page="${page - 1}"
        >
            <span class="arrow prev-arrow">&#10229;</span>
            <span class="text prev-text">prev</span>
        </div>
        <div 
            class="next pagination-item ${!totalCount || page === Math.ceil(totalCount / perPage) ? 'disabled' : ''}" 
            page="${page + 1}" 
            perPage="${perPage}" 
            totalCount="${totalCount}"
        >
            <span class="text next-text">next</span>
            <span class="arrow next-arrow">&#10230;</span>
        </div>
    </div>
`)